# VPSCheap High Fidelity Domain Installer #

This is a script that uses [OmegaHeron's](https://forums.highfidelity.com/u/omegaheron/summary) Ubuntu 16.04 [High Fidelity Domain Packages](https://forums.highfidelity.com/t/updated-ubuntu-xenial-package/13059/) and [Cracker.Hax's](https://forums.highfidelity.com/u/cracker.hax/summary) [systemd setup](https://forums.highfidelity.com/t/setting-assignment-client-and-domain-server-as-a-service-in-systemd-ubuntu/13124) for High Fidelity to provide an easy quick setup and deployment of a High Fidelity Domain.

## Getting Started ##

### VPSCheap Setup ###

Since this script was designed for a [VPSCheap.net](https://www.vpscheap.net/) server this guide will provide a more precise step to step guide for users who are using this service.
We have assumed that you have created your server and are running Ubuntu 16.04. If this is not the case (or VPSCheap has decided to not have the 16.04 image in their setup like I have noticed, so just choose the 16.10) you can rebuild your VPS to this image by logging in, selecting your VPS and clicking the rebuild button at the bottom of the control panel.

![vpscheap-config-rebuild](https://bitbucket.org/Vargink/vpscheap-high-fidelity-domain-installer/raw/master/img/vpscheap-config-rebuild.png)

Then selecting Ubuntu 16.04 and pressing the build button at the bottom of the page again.

![vpscheap-config-rebuild-select](https://bitbucket.org/Vargink/vpscheap-high-fidelity-domain-installer/raw/master/img/vpscheap-config-rebuild-select.png)

The VPS should be rebuilt quickly and after this go back to the Details area of your login and grab the IP Address of your VPS and the Password

![vpscheap-config-details](https://bitbucket.org/Vargink/vpscheap-high-fidelity-domain-installer/raw/master/img/vpscheap-config-details.png)

It is strongly recommended that you change this password so now would be a good time to do so.

### Connecting to Your VPS ###

We will be using a tool called [putty](http://www.putty.org/) to connect to your server you can download it for your computer [here](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html). Once you have this downloaded and running put the Main IP from your control panel into the Host Name area of putty, make sure SSH is selected and press the Open button.

![putty-settings](https://bitbucket.org/Vargink/vpscheap-high-fidelity-domain-installer/raw/master/img/putty-settings.png)

You should have a black screen appear not asking for a username. Put in your desired username (For the VPSCheap people this is root) hit enter and then put in your password. 

![putty-login](https://bitbucket.org/Vargink/vpscheap-high-fidelity-domain-installer/raw/master/img/putty-login.png)

If all goes well you should now have a terminal! It’s strait forward after this. All you need to do is copy and paste the following line into your terminal (hint: Right Clicking in putty pastes what’s in your clipboard) hit enter and follow the menu! Happy installing! If you need to use this again, after running the below line you can just type in 'dirty-kurty' into putty and get that menu again.

```sudo wget -O - https://bitbucket.org/Vargink/vpscheap-high-fidelity-domain-installer/raw/master/hifi-domain-installer.sh | sudo bash```

![putty-paste-line](https://bitbucket.org/Vargink/vpscheap-high-fidelity-domain-installer/raw/master/img/putty-paste-line.gif)

### What About Backup? ###

So now you have your domain up and running and now you're asking. "How do i backup stuff!" We will be using [filezilla](https://filezilla-project.org/) to connect to your VPS and get your domains backup. You can download the version you want [here](https://filezilla-project.org/download.php?type=client) if you enter Main IP, Username (root), Password and the Port of 22 and hit Quickconnect.

![putty-paste-line](https://bitbucket.org/Vargink/vpscheap-high-fidelity-domain-installer/raw/master/img/filezilla-connect.png)

Your domains files are located in /home/hifi/.local/High Fidelity backing up this folder should get everything you have.

### Can I Host Files? ###

You sure can! Apache is installed by default (if you're using VPSCheap) if you goto /var/www/html this is your parent website directory. Anything you put into this directory will work with http://(Main IP)/

## Disclaimer ##

This script comes without any warranty and I am not associated with VPSCheap or any parent company etc etc.

### Who do I talk to? ###

* [Kurtis Anatine](https://keybase.io/theguywho)