#!/bin/bash

# Dirty Kurty installer
# 
# This is the installer for the dirty kurty
# 
# Ubuntu Package made and maintained by OmegaHeron https://forums.highfidelity.com/u/omegaheron/summary
# systemd implimentation by Cracker.Hax https://forums.highfidelity.com/u/cracker.hax/summary
# Thrown together in this installer by Kurtis Anatine https://keybase.io/theguywho


if [ "$EUID" -ne 0 ]
  then echo "Please run me as root you can do this by typing sudo in front of the command"
  exit
fi
function fail {
    echo $1 >&2
    exit 1
}

function botsay {
    local stra=$1
    local strb=$2
    local lena=${#stra}
    local lenb=${#strb}
    if (( lena < lenb )); 
        then
            for ((lena;lena<lenb;lena++))
            do
                stra+=" "
            done
        else
            for ((lenb;lenb<lena;lenb++))
            do
                strb+=" "
            done
    fi
    local filler
    for ((i=0;i<lena;i++)) do
        filler+="="
    done
    clear
    echo '
                          
     `:+yssoo++//::--..  
   ./oosdhyyyyyhhhhhhdh` 
  -ossyhmdhhyyssssssydd` 
  :ymNmNdhsssymmmdyyymmm:
  :dhhymdhsssoyhhysssdmm      +='"$filler"'=+
  :hhhydmdyysyhssdsssdmy      | '"$stra"' |
  :hhhhhmdyyysssssyyhdm.      | '"$strb"' |
  -hhhhhmmmmmmmmmmmmmmm-      +='"$filler"'=+
  .syhddmmmmmmmmddyso+/` 
      `+dmmmmmmo         
      /dNdyyyyyh.        
      hddhyyhhhhs        
      -yyhhddddh.        
       +hdddmmmd/        
       ydmh/:dNms        
       +s+   `::`        
                         
'
}
function retry {
  local n=1
  local max=5
  local delay=15
  while true; do
    "$@" && break || {
      if [[ $n -lt $max ]]; then
        ((n++))
        botsay "Oh man something went wrong, lets try again in $delay seconds!" "Try: $n/$max"
        sleep $delay;
      else
        fail "Something went wrong! Here is the error D:"
      fi
    }
  done
}
botsay "Yo! I'm installing myself" "Just give me one second"
retry apt-get -qq update
retry apt-get -qq install git
retry git clone https://bitbucket.org/Vargink/vpscheap-high-fidelity-domain-installer.git /opt/dirty-kurty
retry ln -s /opt/dirty-kurty/bin/dirty-kurty /usr/local/bin/dirty-kurty
retry chmod +x /usr/local/bin/dirty-kurty

botsay "Done!" "Just type in sudo dirty-kurty to get started"
exit 1